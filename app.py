import numpy as np
import flask
from flask import render_template
import pickle


app = flask.Flask(__name__, template_folder='templates')

@app.route('/', methods=['POST', 'GET'])

@app.route('/index', methods=['POST', 'GET'])

def main():
    if flask.request.method == 'GET':
        return render_template('main.html')

    if flask.request.method == 'POST':
        with open('model_tree2.pkl', 'rb') as f:
            loaded_model = pickle.load(f)
        
        IW = float(flask.request.form['IW'])
        IF = float(flask.request.form['IF'])
        VW = float(flask.request.form['VW'])

        x = [[IW, IF, VW]]

        y_pred = loaded_model.predict(x)

        for i in y_pred: 
            y_str = str(f"Глубина: {float('{0:.4f}'.format(i[0]))} Ширина: {chr(10)} {float('{0:.4f}'.format(i[1]))}")


        return render_template('main.html', result=y_str)

if __name__ == '__main__':
    app.debug = True

    app.run()